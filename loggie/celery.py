import os
import sentry_sdk
from celery import Celery
#from sentry_sdk.integrations.celery import CeleryIntegration


app = Celery('loggie', backend='rpc')
app.config_from_object('django.conf:settings')

CELERY_ENABLE_UTC = True
CELERY_IGNORE_RESULT = True
CELERY_TIMEZONE = "UTC"
CELERY_TASK_RESULT_EXPIRES = 180

BROKER_USER = os.environ.get("BROKER_USER")
BROKER_PASSWORD = os.environ.get("BROKER_PASSWORD")
BROKER_HOST = os.environ.get("BROKER_HOST")
BROKER_PORT = os.environ.get("BROKER_PORT")
BROKER_VHOST = os.environ.get("BROKER_VHOST")

BROKER_URL = f"amqp://{BROKER_USER}:{BROKER_PASSWORD}@{BROKER_HOST}:{BROKER_PORT}/{BROKER_VHOST}"

CELERY_EMAIL_TASK_CONFIG = {
    'queue': 'email',
    'rate_limit': '10/m',
    'name': 'djcelery_email_send'
}

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()

task_routes = {
    'billing.tasks': 'billing',
    'customers.tasks.reset_axes_logs': 'low_priority',
}

# Celery beat
app.conf.beat_schedule = {
    'money_exchange_rates': {
        'task': 'billing.tasks.update_exchange_rates',
        'schedule': 43200
    },
    'create_schedule_invoices': {
        'task': 'billing.tasks.create_schedule_invoices',
        'schedule': 3600
    },
    'confirm_invoices': {
        'task': 'billing.tasks.confirm_invoices',
        'schedule': 15
    },
    'reset_axes_logs': {
        'task': 'customers.tasks.reset_axes_logs',
        'schedule': 36000
    },
    'clear_jwt_blacklist': {
        'task': 'customers.tasks.clear_jwt_blacklist',
        'schedule': 36000
    }
}

# Sentry
CELERY_DSN = os.environ.get("CELERY_DSN")
#sentry_sdk.init(CELERY_DSN, integrations=[CeleryIntegration()])

from loggie.secret_settings import FLOWER_ENV, \
                                   FLOWER_USER_STAS, FLOWER_PASS_STAS, \
                                   FLOWER_USER_DMI, FLOWER_PASS_DMI

# url_prefix = 'flower'

# Can not be done before socket is 600
# unix_socket = '/tmp/flower.sock'

basic_auth = [f'{FLOWER_USER_STAS}:{FLOWER_PASS_STAS}', f'{FLOWER_USER_DMI}:{FLOWER_PASS_DMI}']

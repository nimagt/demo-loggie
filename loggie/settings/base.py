from __future__ import absolute_import, unicode_literals

import os
from os import environ
from os.path import abspath, dirname, join
from django.utils.translation import gettext_lazy as _

from django.core.exceptions import ImproperlyConfigured


def get_env_variable(var_name, default='', required=True):
    try:
        return environ[var_name]
    except KeyError:
        if required:
            error_msg = "Set the {} environment variable.".format(var_name)
            raise ImproperlyConfigured(error_msg)
        else:
            return default


PROJECT_ROOT = dirname(dirname(dirname(abspath(__file__))))
PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SITE_ID = 1

INSTALLED_APPS = [
    'django.contrib.sites',
    'django.contrib.auth',
    'django.contrib.gis',
    'django.contrib.admin',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'rest_framework',
    'rest_framework_api_key',
    'django_elasticsearch_dsl',
    'celery',
    'djoser',
    'rest_framework_simplejwt.token_blacklist',
    'axes',
    'django_filters',
    'cities',
    'djmoney',
    'djmoney.contrib.exchange',
    'drf_yasg',
    'anymail',
    'djcelery_email',
    'invitations',

    'customers',
    'billing',
    'logistic_base',
    'search_indexes',
]

# Auth
AUTHENTICATION_BACKENDS = [
    # AxesBackend should be the first backend in the AUTHENTICATION_BACKENDS list.
    'axes.backends.AxesBackend',
    'django.contrib.auth.backends.ModelBackend',
]
AXES_FAILURE_LIMIT = 42
AXES_COOLOFF_TIME = 3


MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # AxesMiddleware should be the last middleware in the MIDDLEWARE list.
    'axes.middleware.AxesMiddleware',
]

ROOT_URLCONF = 'loggie.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['api'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_simplejwt.authentication.JWTAuthentication',
        # 'rest_framework.authentication.SessionAuthentication',
    ),

    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 1000,

    'DEFAULT_SCHEMA_CLASS': 'rest_framework.schemas.coreapi.AutoSchema'
}

# Djmoney
CURRENCIES = ('USD', 'EUR', 'RUB', 'CNY', 'JPY')
BASE_CURRENCY = 'USD'
EXCHANGE_BACKEND = 'djmoney.contrib.exchange.backends.FixerBackend'
FIXER_URL = 'http://data.fixer.io/api/latest?symbols=USD,EUR,RUB,CNY,JPY'


# Django-cities
CITIES_LOCALES = ['en', 'ru', 'jp', 'zh', 'ko']
CITIES_POSTAL_CODES = []
CITIES_FILES = {
    # ...
    'city': {
       'filename': 'cities5000.zip',
       'urls':     ['http://download.geonames.org/export/dump/'+'{filename}']
    },
    # ...
}


DJOSER = {
    # urls with placeholders in frontend needed
    'PASSWORD_RESET_CONFIRM_URL': 'customers/me/password/reset/confirm/{uid}/{token}',
    'SEND_ACTIVATION_EMAIL': True,
    'SEND_CONFIRMATION_EMAIL': False,
    'ACTIVATION_URL': 'customers/me/activate/{uid}/{token}',
    'SET_USERNAME_RETYPE': False,
    'PASSWORD_RESET_CONFIRM_RETYPE': False,
    'LOGOUT_ON_PASSWORD_CHANGE': False,
    'PASSWORD_RESET_SHOW_EMAIL_NOT_FOUND': False,
    'TOKEN_MODEL': None,  # needed for JWT
    'EMAIL': {
        'activation': 'djoser.email.ActivationEmail',
        'confirmation': 'djoser.email.ConfirmationEmail',
        'password_reset': 'djoser.email.PasswordResetEmail',
        },
    'PERMISSIONS': {
        'user_delete': ['rest_framework.permissions.IsAdminUser'],
        },
    'SERIALIZERS': {
        'user_create': 'customers.serializers.CustomerCreateSerializer',
        'user': 'customers.serializers.CustomerSerializer',
        'current_user': 'customers.serializers.CustomerExcessSerializer',
        }
}

SIMPLE_JWT = {
        'ROTATE_REFRESH_TOKENS': True,
        'BLACKLIST_AFTER_ROTATION': True,
}


# Anymail
SITE_NAME = 'loggie.ru'
DOMAIN = 'loggie.ru'
DEFAULT_FROM_EMAIL = 'Loggie <noreply@loggie.ru>'
EMAIL_BACKEND = "djcelery_email.backends.CeleryEmailBackend"

# Django Celery Email
CELERY_EMAIL_BACKEND = "anymail.backends.sendgrid.EmailBackend"

# Invitations
INVITATIONS_INVITATION_EXPIRY = 30
INVITATIONS_INVITATION_MODEL = 'customers.CompanyInvite'

# Elasticsearch
ELASTICSEARCH_DSL_AUTOSYNC = False
ELASTICSEARCH_DSL_AUTO_REFRESH = False
# Нужно добавить delayed сигнал
# ELASTICSEARCH_DSL_SIGNAL_PROCESSOR = 'django_elasticsearch_dsl.signals.RealTimeSignalProcessor'

# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGES = [
    ('en', _('English')),
    ('ru', _('Russian')),
]

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

STATIC_ROOT = join(PROJECT_ROOT, 'static')
STATIC_URL = '/static/'

MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'media')
MEDIA_URL = '/media/'

AUTH_USER_MODEL = 'customers.CompanyUser'

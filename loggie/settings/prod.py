from __future__ import absolute_import, unicode_literals
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

from .base import *
from loggie.secret_settings import *

DEBUG = False

LOCALE_PATHS = (
    '/opt/web/loggie-users/locale',
)

WSGI_APPLICATION = 'loggie.wsgi.application'

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_simplejwt.authentication.JWTAuthentication',
        # 'rest_framework.authentication.SessionAuthentication',
    ),
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
    ),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 100,

    'DEFAULT_SCHEMA_CLASS': 'rest_framework.schemas.coreapi.AutoSchema'
}


ELASTICSEARCH_INDEX_NAMES = {
    'search_indexes.documents.terminal': 'prod_terminal',
}


DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'loggie',
        'USER': DB_USER,
        'PASSWORD': DB_PASS,
        'CONN_MAX_AGE': 600,
        'HOST': DB_HOST,
        'PORT': DB_PORT,
        'DISABLE_SERVER_SIDE_CURSORS': True,
        'OPTIONS': {
                  'sslmode':'verify-ca',
                  'sslrootcert': os.path.join(PROJECT_DIR, 'ca-certificate.crt')
        },
    }
}


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': '/var/log/uwsgi/loggie.log',
            'formatter': 'verbose',
            'maxBytes': 1024 * 1024 * 50,
        },
       # 'mail_admins': {
       #     'level': 'ERROR',
       #     'class': 'django.utils.log.AdminEmailHandler'
       # },
    },
    'loggers': {
        'django': {
            'handlers': ['file'],
            'propagate': True,
            'level': 'ERROR',
        },
        'core': {
            'handlers': ['file'],
            'propagate': True,
            'level': 'ERROR',
        }
    }
}


# Sentry
sentry_sdk.init(dsn=DJANGO_DSN, integrations=[DjangoIntegration()])

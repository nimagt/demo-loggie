from __future__ import absolute_import, unicode_literals
import os

from .base import *


DEBUG = True

ALLOWED_HOSTS = ['127.0.0.1', '192.168.0.3', 'testserver', 'localhost']

LOCALE_PATHS = (
    '/code/locale',
)

# Dev Keys
# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

SECRET_KEY = '%=ibm7w!+o#dv4b5m$j7@5ysv*4hfrip_pl)1*83z7b4985z&ms'
ANYMAIL_SENDGRID_API_KEY = "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"
FIXER_ACCESS_KEY = '99dffb42e5f65ea06bc03e4458b449e0'

# Celery
CELERY_ENV = 'loggie.settings.dev'
BROKER_URL = os.environ.get("BROKER_URL")


DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'loggie',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'db',
        'PORT': 5432,
        'TEST': {
            'NAME': 'test_loggie',
        },
    }
}


ELASTICSEARCH_DSL = {
    'default': {
        'hosts': 'elasticsearch:9200',
    },
}

ELASTICSEARCH_INDEX_NAMES = {
    'search_indexes.documents.terminal': 'terminal',
}

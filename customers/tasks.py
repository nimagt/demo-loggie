from django.utils import timezone

from celery import shared_task
from axes.utils import AxesProxyHandler
from rest_framework_simplejwt.token_blacklist.models import OutstandingToken
# from invitations.adapters import get_invitations_adapter


# @shared_task
# def send_invitation_email(data, email_template):
#     '''
#     Send Invite email to user.
#     '''
#     get_invitations_adapter().send_mail(email_template, data['email'], data)


@shared_task
def reset_axes_logs():
    '''
    Reset Bad attemts login IP.
    '''
    AxesProxyHandler.reset_logs(age_days=30)


@shared_task
def clear_jwt_blacklist():
    '''
    Clear blacklisted rotated JWT.
    '''
    OutstandingToken.objects.filter(expires_at__lte=timezone.now()).delete()

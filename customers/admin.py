from django.contrib import admin
from django.contrib.auth.models import Group
from rest_framework_api_key.admin import APIKeyModelAdmin

from .models import Company, CompanyAPIKey,  CompanyUser, \
                    CompanySubscription, CompanyInvite

from billing.models import CompanyPrepaid


@admin.register(CompanyAPIKey)
class CompanyAPIKeyModelAdmin(APIKeyModelAdmin):
    autocomplete_fields = ['company']


class CompanyPrepaidAdmin(admin.TabularInline):
    model = CompanyPrepaid
    readonly_fields = ['balance']

    def has_delete_permission(self, request, obj=None):
        return False


class CompanyUserInlineAdmin(admin.TabularInline):
    model = CompanyUser
    fields = ('username', 'first_name', 'last_name', 'email', 'is_admin',
              'phone_number')
    readonly_fields = ('username', 'first_name', 'last_name', 'email',
                       'is_admin', 'phone_number')
    extra = 0

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False


class CompanyInviteInlineAdmin(admin.TabularInline):
    model = CompanyInvite
    extra = 0


class CompanySubscriptionInlineAdmin(admin.TabularInline):
    model = CompanySubscription
    fk_name = 'company'
    autocomplete_fields = ['carrier']
    extra = 0


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    model = Company
    list_display = ['name', 'get_balance', 'user_number', 'is_carrier',
                    'is_prepaid']
    list_filter = ['is_carrier']
    search_fields = ['name', 'email']
    inlines = (CompanyUserInlineAdmin,
               CompanyPrepaidAdmin,
               CompanySubscriptionInlineAdmin)

    def get_balance(self, obj):
        return obj.prepaid.balance_usd
    get_balance.short_description = 'Balance'

    def is_prepaid(self, obj):
        return obj.prepaid.prepaid
    is_prepaid.short_description = 'Prepaid'
    is_prepaid.boolean = True


@admin.register(CompanyUser)
class UserAdmin(admin.ModelAdmin):
    exclude = ('user_permissions', 'groups')
    list_display = ('username', 'email', 'phone_number', 'company',
                    'is_admin', 'is_active')
    list_filter = ('is_admin', 'is_active')
    search_fields = ('username', 'email', 'first_name', 'last_name')
    autocomplete_fields = ['company']


admin.site.unregister(Group)

from django.dispatch import receiver
from django.db.models.signals import post_save, post_delete

##from rates.models import RateKit, RateKitSubscription

from .models import Company, CompanySubscription


'''
Signals on connected Company models
Carrier public RateKits are created here
'''


##@receiver(post_delete, sender=CompanySubscription)
##def delete_carriers_transfered(sender, instance, **kwargs):
##    """
##    Signal delete all RateKit subscriptions on Carrier unsubscribe.
##    """
##    company = instance.company
##    carrier = instance.carrier
##    rate_kits = RateKitSubscription.objects.filter(rate_kit__company=carrier,
##                                                   company=company)
##    # удалять не обязательно
##    # rate_kits.delete()


'''
Signals used to create or archive Public RateKits while making company carrier
'''


# @receiver(post_save, sender=Company)
# def create_public_ratekit(sender, instance, **kwargs):
#    if instance.is_carrier is True:
#        public_ratekit, created = RateKit.objects.get_or_create(
#                company=instance,
#                is_public=True,
#                defaults={'title': instance.name}
#        )
#        if not created:
#            public_ratekit.archived = False
#            public_ratekit.save()


#@receiver(post_save, sender=Company)
#def archive_public_ratekit(sender, instance, **kwargs):
#    public_ratekits = RateKit.objects.filter(company=instance,
#                                             is_public=True,
#                                             archived=False)
#    if instance.is_carrier is False and public_ratekits.exists():
#        public_ratekits.update(archived=True)

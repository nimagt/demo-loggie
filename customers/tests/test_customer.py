from django.conf import settings
from django.core import mail
from django.test.utils import override_settings
from djet import assertions, restframework
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

import djoser.views
from djoser.conf import settings as default_settings
from djoser.compat import get_user_email



from api.views import *
from customers.models import CompanyUser


def create_user(use_custom_data=False, **kwargs):
    data = (
        {
            "username": "",
            "first_name": "John",
            "last_name": "Doe",
            "password": "SDG43gfdv34rgbdfbh",
            "email": "john@mail.ru",
            "phone_number": "+79089925844",
        }
    )
    data.update(kwargs)
    user = CompanyUser.objects.create_user(**data)
    user.raw_password = data["password"]
    return user


class UserCreateViewTest(
    APITestCase,
    assertions.StatusCodeAssertionsMixin,
    assertions.EmailAssertionsMixin,
    assertions.InstanceAssertionsMixin,):

    view_class = CustomerList

    def test_post_create_user_without_login(self):
        data =  {"username": "john",
                 "first_name": "John",
                 "last_name": "Doe",
                 "password": "SDG43gfdv34rgbdfbh",
                 "email": "test@test.ru",
                 "phone_number": "+79089925844"}

        response = self.client.post(path=reverse("customer-list"), data=data)
        print(response)
        self.assert_status_equal(response, status.HTTP_201_CREATED)
        self.assertTrue("password" not in response.data)

        self.assert_instance_exists(CompanyUser, username=data["username"])
        user = CompanyUser.objects.get(username=data["username"])
        self.assertTrue(user.check_password(data["password"]))


    @override_settings(
        DJOSER=dict(
            settings.DJOSER,
            **{"SEND_ACTIVATION_EMAIL": True, "SEND_CONFIRMATION_EMAIL": False}
        ),
        EMAIL_BACKEND='anymail.backends.test.EmailBackend'
    )
    def test_post_create_user_with_login_and_send_activation_email(self):
        data =  {"username": "john",
                 "first_name": "John",
                 "last_name": "Doe",
                 "password": "SDG43gfdv34rgbdfbh",
                 "email": "test@test.ru",
                 "phone_number": "+79089925844"}
        response = self.client.post(reverse("customer-list"), data=data)
        print(len(mail.outbox))
        self.assert_status_equal(response, status.HTTP_201_CREATED)
        self.assert_instance_exists(CompanyUser, username=data["username"])
        self.assertEqual(len(mail.outbox), 1)

        self.assertEqual(mail.outbox[0].to, [data["email"]])
        self.assert_email_exists(to=[data["email"]])

        user = CompanyUser.objects.get(username="john")
        self.assertFalse(user.is_active)

    def test_post_not_create_new_user_if_username_exists(self):
        create_user(username="john")
        data =  {"username": "john",
                 "first_name": "John",
                 "last_name": "Doe",
                 "password": "SDG43gfdv34rgbdfbh",
                 "email": "test@test.ru",
                 "phone_number": "+79089925844"}
        response = self.client.post(reverse("customer-list"), data=data)

        self.assert_status_equal(response, status.HTTP_400_BAD_REQUEST)

    def test_post_not_create_new_user_if_email_exists(self):
        create_user(username="john", email="test@test.ru")
        data =  {"username": "jack",
                 "first_name": "Jack",
                 "last_name": "Doe",
                 "password": "SDG43gfdv34rgbdfbh",
                 "email": "test@test.ru",
                 "phone_number": "+79089925844"}
        response = self.client.post(reverse("customer-list"), data=data)

        self.assert_status_equal(response, status.HTTP_400_BAD_REQUEST)
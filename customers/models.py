from django.db import models
from django.contrib.auth.models import AbstractUser
from rest_framework.exceptions import AuthenticationFailed
from phonenumber_field.modelfields import PhoneNumberField
from invitations.models import Invitation
from rest_framework_api_key.models import AbstractAPIKey


'''
User and Company models excluding billing
'''


class Company(models.Model):
    name = models.CharField(max_length=20, null=False, blank=False)
    email = models.EmailField(null=False, blank=False)
    phone_number = PhoneNumberField(null=True, blank=True)
    logotype = models.ImageField(blank=True, null=True,
                                 upload_to='images/logotypes')
    description = models.TextField(blank=True)
    public_logotype = models.ImageField(blank=True, null=True,
                                        upload_to='images/public_logotypes')
    is_carrier = models.BooleanField(default=False)

    def __str__(self):
        return '{}'.format(self.name)

    @property
    def user_number(self):
        return self.user_list.count()

    @classmethod
    def get_company(cls, request):
        if request.user.is_authenticated:
            return request.user.company
        else:
            key = request.headers.get("AUTHORIZATION")
            if key:
                try:
                    api_key = CompanyAPIKey.objects.get_from_key(key)
                except:
                    raise AuthenticationFailed
                return Company.objects.get(api_key=api_key)
        return None


class CompanyAPIKey(AbstractAPIKey):
    company = models.ForeignKey(
        Company,
        on_delete=models.CASCADE,
        related_name="api_key",
    )


class CompanyDescription(models.Model):
    '''
    Used for different languages company descriptions.
    '''
    company = models.ForeignKey(Company, related_name='alt_descriptions',
                                on_delete=models.CASCADE)
    description = models.TextField()
    language_code = models.CharField(max_length=100)

    def __str__(self):
        return '{} Alternative Description'.format(self.company)


class CompanyUser(AbstractUser):
    company = models.ForeignKey(Company, on_delete=models.CASCADE,
                                related_name='user_list', null=True)
    is_admin = models.BooleanField(default=False)
    email = models.EmailField(unique=True, null=False, blank=False)
    phone_number = PhoneNumberField(null=True, blank=True)
    # используется для модерирования ставок (только для обслуживания сайта)
    is_moderator = models.BooleanField(default=False)


class CompanyInvite(Invitation):
    company = models.ForeignKey(Company, on_delete=models.CASCADE,
                                related_name='invite_list')

    def __str__(self):
        return '{} Invite'.format(self.company)


class CompanySubscription(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE,
                                related_name='carrier_list')
    carrier = models.ForeignKey(Company, on_delete=models.CASCADE,
                                related_name='subscriber_list')

    class Meta:
        unique_together = ('carrier', 'company')

    def __str__(self):
        return '{} Carrier'.format(self.company)

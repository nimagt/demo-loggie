from django.utils.translation import gettext_lazy as _
from django.core import exceptions as django_exceptions
from django.contrib.auth.password_validation import validate_password
from django.db import transaction
from rest_framework import serializers

from djoser.serializers import UserCreateSerializer
from djoser.conf import settings

# from invitations.adapters import get_invitations_adapter

from .models import Company, CompanyUser, CompanySubscription, \
                    CompanyInvite


errors = {
    "already_invited": _("Invitation have been sent to this email"
                         " address."),
    "already_accepted": _("Owner of this email address has already"
                          " accepted an invite."),
    "email_in_use": _("An active user is using this email address."),
    "not_carrier": _("Company you want to subscribe is not carrier."),
    "duplicate_sub": _("You are already subscribed."),
    "company_exists": _("Only one company per user allowed."),
    "no_company": _("You have no company."),
    "no_invitation": _("You are not invited."),
}


class CompanySerializer(serializers.ModelSerializer):

    class Meta:
        model = Company
        fields = ['id', 'name', 'email', 'logotype', 'description',
                  'public_logotype', 'is_carrier', 'phone_number']
        read_only_fields = ['public_logotype', 'is_carrier']

    def validate(self, data):
        user = self.context['request'].user
        if user.company:
            raise serializers.ValidationError(errors['company_exists'])
        return data


class CompanyUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Company
        fields = ['id', 'name', 'email', 'logotype', 'description',
                  'public_logotype', 'is_carrier', 'phone_number']
        read_only_fields = ['public_logotype', 'is_carrier']


class CustomerSerializer(serializers.ModelSerializer):

    class Meta:
        model = CompanyUser
        fields = ['id', 'username', 'first_name', 'last_name', 'email',
                  'is_admin', 'phone_number']
        read_only_fields = ['username', 'email', 'is_admin']


class CustomerExcessSerializer(serializers.ModelSerializer):
    company = CompanySerializer(many=False, read_only=True)
    prepaid = serializers.BooleanField(source='company.prepaid.prepaid',
                                       read_only=True)

    class Meta:
        model = CompanyUser
        fields = ['id', 'username', 'first_name', 'last_name', 'email',
                  'is_admin', 'phone_number', 'company', 'prepaid']


class CustomerCreateSerializer(UserCreateSerializer):
    invite_key = serializers.CharField(required=False)
    email = serializers.EmailField(required=False)

    class Meta:
        model = CompanyUser
        fields = ['username', 'email', 'password', 'invite_key']
        write_only_fields = ['password', 'invite_key']

    def validate(self, attrs):
        username = attrs.get('username')
        password = attrs.get('password')
        if 'invite_key' in attrs:
            key = attrs.get('invite_key')
            # если пользователь уже зарегистрирован?
            try:
                invite = CompanyInvite.objects.get(key=key)
            except CompanyInvite.DoesNotExist:
                raise serializers.ValidationError(errors['no_invitation'])
            except CompanyUser.objects.filter(email=invite.email).exists:
                raise serializers.ValidationError(errors['email_in_use'])
            email = invite.email
        else:
            if not attrs.get('email'):
                required = serializers.Field.default_error_messages['required']
                raise serializers.ValidationError({"email": required})
            email = attrs.get('email')
        user = CompanyUser(username=username, email=email, password=password)
        try:
            validate_password(password, user)
        except django_exceptions.ValidationError as e:
            serializer_error = serializers.as_serializer_error(e)
            raise serializers.ValidationError(
                {"password": serializer_error["non_field_errors"]}
            )

        return attrs

    def perform_create(self, validated_data):
        if 'invite_key' in validated_data:
            with transaction.atomic():
                key = validated_data.pop('invite_key')
                invite = CompanyInvite.objects.get(key=key)
                user = CompanyUser.objects.create_user(**validated_data,
                                                       email=invite.email,
                                                       company=invite.company)
                user.is_active = False
                user.company = invite.company
                user.save()
                invite.accepted = True
                invite.save()
            return user
        with transaction.atomic():
            user = CompanyUser.objects.create_user(**validated_data)
            if settings.SEND_ACTIVATION_EMAIL:
                user.is_active = False
                user.save(update_fields=["is_active"])
        return user


class InvitationWriteSerializer(serializers.ModelSerializer):

    class Meta:
        model = CompanyInvite
        fields = ['company']

    def validate_email(self, email):
        if CompanyInvite.objects.all_valid().filter(email__iexact=email,
                                                    accepted=False):
            raise serializers.ValidationError(errors["already_invited"])
        if CompanyInvite.objects.filter(email__iexact=email, accepted=True):
            raise serializers.ValidationError(errors["already_accepted"])
        return email

    def validate(self, data):
        user = self.context['request'].user
        if not user.company:
            raise serializers.ValidationError(errors['no_company'])
        return data

    def create(self, validate_data):
        inviter = self.context['request'].user
        invite = CompanyInvite.create(**validate_data,
                                      inviter=inviter,
                                      company=inviter.company)
        if not CompanyUser.objects.filter(email=invite.email).exists:
            pass  # шлём email c кодом
        else:
            pass  # шлём email о том, что вас пригласили
        return invite


class InvitationReadSerializer(serializers.ModelSerializer):

    class Meta:
        model = CompanyInvite
        fields = '__all__'
        read_only_fields = ['company']


class UserInviteSerializer(serializers.ModelSerializer):

    class Meta:
        model = CompanyInvite
        fields = ['company', 'created', 'accepted']
        read_only_fields = ['company', 'created']

    def update(self, instance, validated_data):
        if validated_data['accepted']:
            instance.accepted = True
            # пользователь становится пользователем компании
            # но что если он уже был в компании?
        return instance


class CompanySubscriptioniListSerializer(serializers.ModelSerializer):
    company = CompanySerializer()
    carrier = CompanySerializer()

    class Meta:
        model = CompanySubscription
        fields = '__all__'


class CompanySubscriptionCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = CompanySubscription
        fields = '__all__'
        read_only_fields = ['company']

    def validate_carrier(self, value):
        company = self.context['request'].user.company
        if not value.is_carrier:
            raise serializers.ValidationError(errors['not_carrier'])
        if CompanySubscription.objects.filter(company=company,
                                              carrier=value).exists():
            raise serializers.ValidationError(errors['duplicate_sub'])
        return value

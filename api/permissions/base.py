from rest_framework import permissions

'''
Base user and admin permissions
'''


class CompanyAdminPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.user.is_admin

    def has_object_permission(self, request, view, obj):
        return request.user.is_admin

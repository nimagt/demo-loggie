from django.urls import path, include
from rest_framework.permissions import IsAdminUser
from djoser.urls.jwt import urlpatterns as jwt_urls

from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from .base import base_urls
from .customers import company_urls, customer_urls, djoser_user_urls

from ..views.base import PublicDocsView


schema_view = get_schema_view(
        openapi.Info(
            title='Loggie API',
            default_version='v0',
            description='Cool Loggie API Schema'),
        public=True,
        # permission_classes=[IsAdminUser]
        )


urlpatterns = [
    path('docs/', schema_view.with_ui('swagger', cache_timeout=0)),
    path('public_docs/', PublicDocsView.as_view(), name='public_docs'),
    path('docs/redoc', schema_view.with_ui('redoc', cache_timeout=0)),
    path('v0/', include(base_urls)),
    path('v0/companies/', include(company_urls)),
    path('v0/customers/', include(customer_urls)),
    path('v0/', include(jwt_urls)),
    path('v0/', include(djoser_user_urls)),
##    path('v0/companies/moderation/', include(company_moderation_urls)),
##    path('v0/companies/', include(company_subscriptions_urls)),

]

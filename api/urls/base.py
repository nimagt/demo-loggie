from django.urls import path

from api.views import TariffList, ContainerList, ExchangeRateList, \
    terminal_autocomplete


base_urls = [
    path('tariff/', TariffList.as_view(), name='tariff_list'),
    path('currency/', ExchangeRateList.as_view(), name='currency_exchange_rates'),
    path('container/', ContainerList.as_view(), name='container_types_list'),
    path('terminal_ac/', terminal_autocomplete, name='terminal_autocomplete'),
]

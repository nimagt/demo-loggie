from django.urls import path
from django.contrib.auth import get_user_model

from rest_framework.routers import DefaultRouter
from djoser.views import UserViewSet

from api.views import CustomerList, CompanyList, CompanyDetail, \
                      CompanyInvites, UserInvites


router = DefaultRouter()
router.register('customers', UserViewSet)

User = get_user_model()

djoser_user_urls = router.urls

company_urls = [
    # placed in companies/
    path('', CompanyList.as_view(), name='companies'),
    path('me/customers/', CustomerList.as_view(), name='my-company-customers'),
    path('<int:pk>', CompanyDetail.as_view(), name='company-detail'),
    path('me/', CompanyDetail.as_view(), name='company-detail_me', kwargs={'pk': 'me'}),
    path('me/invites/', CompanyInvites.as_view(), name='company-invites', kwargs={'pk': 'me'}),
    path('me/invitations/<int:pk>/', CompanyInvites.as_view(), name='company-invite-detail'),
]

customer_urls = [
    # placed in customers/
    path('me/invites/', UserInvites.as_view(), name='user-invites', kwargs={'pk': 'me'}),
    path('me/invitations/<int:pk>/', UserInvites.as_view(), name='user-invite-detail'),
]

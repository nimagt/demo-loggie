from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from api.pagination import SmallResultsSetPagination

from billing.models import DebitInvoice, CreditInvoice
from billing.serializers import CompanyPlanedTariffSerializer, \
    CompanyPrepaidSerializer, DebitInvoiceSerializer, CreditInvoiceSerializer

from api.permissions import CompanyAdminPermission


'''
Views for company billing update, invoices and payments
'''


class CompanyTariffUpdate(generics.RetrieveUpdateAPIView):
    '''
    Users Company Tariff Update.
    '''
    serializer_class = CompanyPlanedTariffSerializer
    permission_classes = [IsAuthenticated, CompanyAdminPermission]

    def get_object(self):
        company = self.request.user.company
        return company.planed_tariff


class CompanyPrepaidUpdate(generics.RetrieveUpdateAPIView):
    '''
    Company Prepaid Data Update. Number of users and auto payemnt.
    '''
    serializer_class = CompanyPrepaidSerializer
    permission_classes = [IsAuthenticated, CompanyAdminPermission]

    def get_object(self):
        company = self.request.user.company
        return company.prepaid


class DebitInvoiceList(generics.ListAPIView):
    '''
    List of Company Debit Invoices. All company payments.
    '''
    serializer_class = DebitInvoiceSerializer
    permission_classes = [IsAuthenticated, CompanyAdminPermission]
    pagination_class = SmallResultsSetPagination

    def get_queryset(self):
        company = self.request.user.company
        return DebitInvoice.objects.filter(company=company)


class CreditInvoiceList(generics.ListAPIView):
    '''
    List of Company Credit Invoices.
    '''
    serializer_class = CreditInvoiceSerializer
    permission_classes = [IsAuthenticated, CompanyAdminPermission]
    pagination_class = SmallResultsSetPagination

    def get_queryset(self):
        company = self.request.user.company
        return CreditInvoice.objects.filter(company=company)


class DebitInvoiceCreateView(generics.CreateAPIView):
    '''
    Create payment for choset tariff.
    '''
    serializer_class = DebitInvoiceSerializer
    permission_classes = [IsAuthenticated, CompanyAdminPermission]

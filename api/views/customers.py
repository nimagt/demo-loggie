from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.http import Http404
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.permissions import SAFE_METHODS

from api.permissions import CompanyAdminPermission

from customers.models import Company, CompanyUser, CompanyInvite
from customers.serializers import CompanySerializer, \
     CompanyUpdateSerializer, CustomerSerializer, InvitationWriteSerializer, \
     InvitationReadSerializer, UserInviteSerializer

from api.pagination import LargeResultsSetPagination


'''
User main information and behavior views
'''


class CustomerList(generics.ListAPIView):
    '''
    List of company users.
    '''
    model = CompanyUser
    serializer_class = CustomerSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        if user.company:
            return CompanyUser.objects.filter(company=user.company)
        return CompanyUser.objects.none()


class CompanyList(generics.ListCreateAPIView):
    '''
    List of carrier companies.
    Creation of company (for not in company users).
    '''
    model = Company
    serializer_class = CompanySerializer
    pagination_class = LargeResultsSetPagination

    def get_permissions(self):
        if self.request.method in SAFE_METHODS:
            return []
        return [IsAuthenticated()]

    def get_queryset(self):
        return Company.objects.filter(is_carrier=True)

    def perform_create(self, serializer):
        user = self.request.user
        serializer.is_valid()
        company = serializer.save()
        user.company = company
        user.is_admin = True
        user.save()


class CompanyDetail(generics.RetrieveUpdateAPIView):
    '''
    Company detail info.
    '''
    model = Company
    serializer_class = CompanyUpdateSerializer

    def get_permissions(self):
        if self.request.method in SAFE_METHODS:
            return []
        return [IsAuthenticated(), CompanyAdminPermission()]

    def get_queryset(self):
        '''
        Can get detail info for not authenticated user.
        '''
        if self.request.user and self.request.user.is_authenticated:
            user = self.request.user
            my = Q(user_list=user)
            carriers = Q(is_carrier=True)
            if self.request.method in SAFE_METHODS:
                return Company.objects.filter(my | carriers)
            return Company.objects.filter(my)
        if not self.request.user or not self.request.user.is_authenticated:
            if self.request.method in SAFE_METHODS:
                return Company.objects.filter(is_carrier=True)
            return Company.objects.none()

    def get_object(self):
        user = self.request.user
        # если not user и pk == me то None
        if self.kwargs.get('pk', None) == 'me' \
                and user and user.is_authenticated:
            company = self.request.user.company
            if company:
                return company
            else:
                raise Http404
        queryset = self.filter_queryset(self.get_queryset())
        obj = get_object_or_404(queryset, pk=self.kwargs.get('pk'))
        self.check_object_permissions(self.request, obj)
        return obj


class CompanyInvites(generics.DestroyAPIView, generics.ListCreateAPIView):
    '''
    List of invites made from users company to users.
    Can be created or destroyed.
    '''
    model = CompanyInvite
    permission_classes = [IsAuthenticated, CompanyAdminPermission]

    def get_serializer_class(self):
        if self.request.method in SAFE_METHODS:
            return InvitationReadSerializer
        return InvitationWriteSerializer

    def get_queryset(self):
        company = self.request.user.company
        queryset = CompanyInvite.objects.filter(company=company)
        return queryset


class UserInvites(generics.RetrieveUpdateDestroyAPIView, generics.ListAPIView):
    '''
    List of user invites. Can accept or destroy someones invitation.
    '''
    model = CompanyInvite
    permission_classes = [IsAuthenticated]
    serializer_class = UserInviteSerializer

    def get_queryset(self):
        email = self.request.user.email
        queryset = CompanyInvite.objects.filter(email=email, approved=False)
        return queryset

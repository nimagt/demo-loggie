from django.views.generic import TemplateView

from elasticsearch_dsl.query import MultiMatch

from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import generics
from djmoney.contrib.exchange.models import Rate

from billing.models import Tariff
from billing.serializers import ExchangeRateSerializer, TariffSerializer

from logistic_base.models import Container
from logistic_base.serializers import ContainerSerializer

from search_indexes.documents.terminal import TerminalDocument

'''
Public information views
'''


class TariffList(generics.ListAPIView):
    '''
    List of tariffs avaliable.
    Company can choose one to get payment-only functional.
    '''
    queryset = Tariff.objects.all()
    serializer_class = TariffSerializer


class ContainerList(generics.ListAPIView):
    '''
    List of avaliable container types.
    '''
    queryset = Container.objects.all().order_by('id')
    serializer_class = ContainerSerializer


class ExchangeRateList(generics.ListAPIView):
    '''
    List of currencies and exchange rates.
    '''
    queryset = Rate.objects.all()
    serializer_class = ExchangeRateSerializer


'''
Views with public information for admin panel
Will be deprecated later
'''


@api_view(['GET'])
def terminal_autocomplete(request):
    q = request.query_params.get('q', None)
    if not q:
        q = ''
    q = q.replace(' ', '')
    status = request.query_params.get('status', None)
    exclude = request.query_params.get('exclude', None)
    exclude_country = request.query_params.get('exclude_country', None)
    ngram_fields = ['en.name.ngram', 'ru.name.ngram',
                    'en.city.name.ngram', 'ru.city.name.ngram',
                    'en.city.country.name', 'ru.city.country.name']
    suggest_fields = ['en.name.suggest', 'ru.name.suggest',
                      'en.city.name.suggest', 'ru.city.name.suggest']
    s = TerminalDocument.search()
    if status:
        s = s.query('match', status=status)  # не срабатывает хайлайт
    if exclude and exclude.isnumeric():
        s = s.exclude('match', id=int(exclude))
    if exclude_country:
        s = s.exclude('match', city__country__code=exclude_country)
    if len(q) < 3:
        multi_match = MultiMatch(query=q, type='bool_prefix',
                                 fields=suggest_fields)
    else:
        multi_match = MultiMatch(query=q, fields=ngram_fields)
    s = s.sort('_score', '-city.population') \
         .query(multi_match)[0:5] \
         .highlight_options(pre_tags='<b>', post_tags='</b>',
                            number_of_fragments=1) \
         .highlight('ru.name.ngram', 'ru.city.name.ngram',
                    'en.name.ngram', 'en.city.name.ngram') \
         .source(['id', 'en', 'ru', 'city', 'status']) \
         .params(filter_path='hits.hits') \
         .execute()
    response_dict = s.to_dict()
    return Response(response_dict['hits']['hits'])


class PublicDocsView(TemplateView):
    template_name = 'public_doc.html'


from django.apps import AppConfig


class ElasticConfig(AppConfig):
    name = 'search_indexes'

from django.conf import settings

from django_elasticsearch_dsl import Document, fields
from django_elasticsearch_dsl.registries import registry

from logistic_base.models import Terminal
from .analyzers import html_strip, edge_ngram_completion, ascii_fold


@registry.register_document
class TerminalDocument(Document):
    """Document Elasticsearch document."""
    id = fields.IntegerField(attr='id')
    name = fields.TextField(
        fields={
            'suggest': fields.TextField(analyzer=ascii_fold),
            'ngram': fields.TextField(analyzer=edge_ngram_completion)
        }
    )

    ru = fields.ObjectField(
        attr='ru_indexing',
        properties={
            'name': fields.TextField(
                fields={
                    'suggest': fields.TextField(analyzer=ascii_fold),
                    'ngram': fields.TextField(analyzer=edge_ngram_completion)
                }
            ),
            'city': fields.ObjectField(
                properties={
                    'name': fields.TextField(
                        fields={
                            'suggest': fields.TextField(analyzer=ascii_fold),
                            'ngram': fields.TextField(analyzer=edge_ngram_completion)
                        }
                    ),
                    'country': fields.ObjectField(
                        properties={
                            'name': fields.TextField(analyzer=edge_ngram_completion)
                        }
                    )
                }
            )
            }
        )
    en = fields.ObjectField(
        attr='en_indexing',
        properties={
            'name': fields.TextField(
                fields={
                    'suggest': fields.TextField(analyzer=ascii_fold),
                    'ngram': fields.TextField(analyzer=edge_ngram_completion)
                }
            ),
            'city': fields.ObjectField(
                properties={
                    'name': fields.TextField(
                        fields={
                            'suggest': fields.TextField(analyzer=ascii_fold),
                            'ngram': fields.TextField(analyzer=edge_ngram_completion)
                        }
                    ),
                    'country': fields.ObjectField(
                        properties={
                            'name': fields.TextField(analyzer=edge_ngram_completion)
                        }
                    )
                }
            )
            }
        )
    city = fields.ObjectField(
        properties={
            'id': fields.IntegerField(),
            'name': fields.TextField(
                analyzer=html_strip,
                fields={
                    'raw': fields.KeywordField(),
                }
            ),
            'population': fields.IntegerField(),
            'location': fields.GeoPointField(attr="location_field_indexing"),
            'country': fields.ObjectField(
                properties={
                    'id': fields.IntegerField(),
                    'name': fields.TextField(
                        analyzer=html_strip,
                        fields={
                            'raw': fields.KeywordField(),
                        }
                    ),
                    'code': fields.TextField(
                        analyzer=html_strip,
                        fields={
                            'raw': fields.KeywordField(),
                        }
                    ),
                }
            )
        }
    )
    status = fields.TextField(attr='status_to_string')

    class Index:
        name = settings.ELASTICSEARCH_INDEX_NAMES[__name__]
        settings = {'number_of_shards': 1,
                    'number_of_replicas': 1,
                    'max_ngram_diff': 3}
        name = 'terminal'

    class Django:
        model = Terminal
        parallel_indexing = True
        queryset_pagination = 50

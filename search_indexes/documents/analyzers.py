from elasticsearch_dsl import analyzer, tokenizer
from elasticsearch_dsl.analysis import token_filter


html_strip = analyzer('html_strip',
                      tokenizer="standard",
                      filter=["lowercase", "snowball"],
                      char_filter=["html_strip"])


edge_ngram_completion_filter = token_filter(
    'edge_ngram_completion_filter',
    type="edge_ngram",
    min_gram=3,
    max_gram=4
)

edge_ngram_completion = analyzer(
    "edge_ngram_completion",
    tokenizer=tokenizer('trigram', 'ngram', min_gram=3, max_gram=4),
    filter=["lowercase"]
)

ascii_fold = analyzer(
    'ascii_fold',
    tokenizer='whitespace',
    filter=[
        'lowercase',
        token_filter('ascii_fold', 'asciifolding')
    ]
)

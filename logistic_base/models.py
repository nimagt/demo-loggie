from django.db import models
from django.db.models import F, OuterRef, Subquery
from django.db.models.functions import Coalesce
from django.utils import translation

from cities.models import City

from .utils import dict_to_obj


'''
Base logistic and geo models
'''


class Container(models.Model):
    type = models.CharField(max_length=20, blank=False, null=False)
    disabled = models.BooleanField(default=False)
    # <21t <24t

    def __str__(self):
        return '{}'.format(self.type)


class ContainerAlternativeName(models.Model):
    container = models.ForeignKey(Container, related_name='alt_names',
                                  on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    language_code = models.CharField(max_length=100)

    def __str__(self):
        return '{} Alternative Name'.format(self.name)


# This is used to index City document
City.location_field_indexing = property(
    lambda self:
    {
        'lat': self.location.y,
        'lon': self.location.x,
    }
)


class CityDetail(City):

    class Meta:
        proxy = True

    def __str__(self):
        return '{}, {}, {}'.format(self.name,
                                   self.region,
                                   self.country)


class TerminalManager(models.Manager):
    def translated(self, lang):
        if lang is None:
            lang = translation.get_language()
        city_name = City.objects.filter(
            alt_names__language_code=lang,
            alt_names__is_historic=False,
            pk=OuterRef('city')
            )\
            .order_by('-alt_names__is_preferred')\
            .values('alt_names__name')[:1]
        term_alt_names = TerminalAlternativeName.objects\
            .filter(language_code=lang,
                    terminal=OuterRef('pk')
                    ).values('name')
        return self.get_queryset()\
            .select_related('city', 'city__country', )\
            .annotate(
                name_l=Coalesce(Subquery(term_alt_names), 'name'),
                country_code=F('city__country__code'),
                city_name_l=Coalesce(Subquery(city_name), F('city__name_std'))
            )


class Terminal(models.Model):
    PORT, STATION, INTRACITY, DROP_STATION = range(4)
    TERMINAL_CHOICES = (
        (PORT, 'Port'),
        (STATION, 'Station'),
        (INTRACITY, 'Intracity'),
        (DROP_STATION, 'DropStation'),
    )
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    name = models.CharField(max_length=50, null=False, blank=False)
    status = models.IntegerField(choices=TERMINAL_CHOICES, default=2)

    objects = TerminalManager()

    def __str__(self):
        return '{}, {}, {}'.format(self.name,
                                   self.city.name,
                                   self.city.country)

    def status_to_string(self):
        '''
        Convert the type field to its string representation.
        Needed for elastic.
        '''
        if self.status == 0:
            return "Port"
        elif self.status == 2:
            return "Intracity"
        elif self.status == 3:
            return "DropStation"
        return "Station"

    @property
    def country_indexing(self):
        wrapper = dict_to_obj({
            'name': self.city.country.name,
            'city': {
                'name': self.city.name
            }
        })
        return wrapper

    def en_indexing(self):
        wrapper = dict_to_obj({
            'name': self.name,
            'city': {
                'name': self.city.name,
                'country': {
                    'name': self.city.country.name
                }
            }
        })
        return wrapper

    @property
    def city_ru_name(self):
        # temporary ru city name absence fix
        city = self.city.alt_names.filter(is_historic=False)
        if city.filter(language_code='ru'):
            return city.filter(language_code='ru').first().name
        return self.city.name_std

    def ru_indexing(self):
        terminal = self.alt_names.filter(language_code='ru')
        country = self.city.country.alt_names.filter(language_code='ru',
                                                     is_historic=False)
        wrapper = dict_to_obj({
            'name': terminal.first().name,
            'city': {
                'name': self.city_ru_name,
                'country': {
                    'name': country.first().name
                }
            }
        })
        return wrapper


class TerminalAlternativeName(models.Model):
    terminal = models.ForeignKey(Terminal, related_name='alt_names',
                                 on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    language_code = models.CharField(max_length=100)

    def __str__(self):
        return '{} Alternative Name'.format(self.terminal)

import json


class Wrapper(object):
    def __str__(self):
        for key, item in self.__dict__.items():
            if isinstance(item, Wrapper):
                return item.__str__()
            else:
                return item

    @property
    def as_dict(self):
        """As dict.

        :return:
        :rtype: dict
        """
        return obj_to_dict(self)

    @property
    def as_json(self):
        return json.dumps(self.as_dict)


def dict_to_obj(mapping):
    wrapper = Wrapper()

    for key, item in mapping.items():
        if isinstance(item, dict):
            setattr(wrapper, key, dict_to_obj(item))
        else:
            setattr(wrapper, key, item)
    return wrapper


def obj_to_dict(obj):
    mapping = {}

    for key, item in obj.__dict__.items():
        if isinstance(item, Wrapper):
            mapping.update({key: obj_to_dict(item)})
        else:
            mapping.update({key: item})

    return mapping

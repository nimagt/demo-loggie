from django.contrib import admin
from django.core.management import call_command

from .models import Container, ContainerAlternativeName, \
                    Terminal, TerminalAlternativeName


class ContainerAlternativeNameInlineAdmin(admin.TabularInline):
    model = ContainerAlternativeName
    extra = 1


@admin.register(Container)
class ContainerAdmin(admin.ModelAdmin):
    list_display = ('type', 'disabled')
    inlines = [ContainerAlternativeNameInlineAdmin]


class TerminalAlternativeNameInlineAdmin(admin.TabularInline):
    model = TerminalAlternativeName
    extra = 1


@admin.register(Terminal)
class TermianlAdmin(admin.ModelAdmin):
    inlines = [TerminalAlternativeNameInlineAdmin]
    list_display = ('name', 'city', 'status')
    list_filter = ('status',)
    autocomplete_fields = ['city']
    search_fields = ('name', 'city__name')
    actions = ['reindex_elastic']

    def reindex_elastic(self, request, queryset):
        call_command('search_index', '--rebuild', '--parallel', '-f')
        self.message_user(request, "Terminals are reindexed.")

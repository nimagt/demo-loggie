from django.utils import translation
from rest_framework import serializers

from djmoney.contrib.exchange.models import Rate
from cities.models import Country, City

from .models import Container, ContainerAlternativeName, Terminal


class ContainerAlternativeName(serializers.ModelSerializer):

    class Meta:
        model = ContainerAlternativeName
        fields = ['name', 'language_code']


class ContainerSerializer(serializers.ModelSerializer):
    alt_names = ContainerAlternativeName(many=True)

    class Meta:
        model = Container
        fields = '__all__'


class ExchangeRateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Rate
        fields = ['currency', 'value']


class CountrySerializer(serializers.ModelSerializer):

    class Meta:
        model = Country
        fields = ['id', 'name', 'code']


class CitySerializer(serializers.ModelSerializer):
    country = CountrySerializer()

    class Meta:
        model = City
        fields = ['id', 'name', 'country']


class TerminalSerializer(serializers.ModelSerializer):
    name_l = serializers.CharField()
    city_name_l = serializers.CharField()
    country_code = serializers.CharField()

    class Meta:
        model = Terminal
        fields = ['id', 'name', 'name_l', 'city_name_l', 'country_code',
                  'status']


class TerminalResultSerializer(serializers.ModelSerializer):
    city = CitySerializer()
    name = serializers.SerializerMethodField()

    class Meta:
        model = Terminal
        fields = ['id', 'name', 'city', 'status']

    def get_name(self, obj):
        lang = translation.get_language()
        translated = obj.alt_names.filter(language_code=lang)
        if not translated:
            return obj.name
        return translated.first().name

# Loggie Django Backend
Loggie backend source code. Welcome here.

## Running Build  
Make backend up.  
`docker-compose up`

## Initial migrations and data import
Don't forget to make initial db migrations and cities import.
Easiest way is to run:  
`docker exec -it  <container-name> /bin/bash` and then:  
`python manage.py makemigrations`  
`python manage.py migrate`  
`python manage.py cities --import=all`  
Optional:
`PYTHONPATH=/code/ django-admin makemessages --settings=loggie.settings.dev`
Or just:
`PYTHONPATH=/code/ django-admin compilemessages --settings=loggie.settings.dev`

## This is `dev` build
Use `--settings=loggie.settings.prod` for prodution.

## Changing models and Elastic indexing
Rebuild elastic documents after model changes.  
`./manage.py search_index --delete -f`  
`./manage.py search_index --create -f`  
`./manage.py search_index --populate -f`  

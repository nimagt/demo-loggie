from datetime import timedelta
from django.db import models

from djmoney.models.fields import MoneyField
from djmoney.contrib.exchange.models import convert_money

from customers.models import Company


'''
Billing models
'''


ADMIN, CARD, TRANSFER = range(3)
CREDIT_CHOICES = (
    (ADMIN, 'Made by admin'),
    (CARD, 'Credit card'),
    (TRANSFER, 'Bank transfer')
)


class Tariff(models.Model):
    price_per_user = MoneyField(max_digits=14, decimal_places=2,
                                default_currency='RUB')
    periodic = models.DurationField(default=timedelta(), blank=False)  # month
    users_min = models.IntegerField(default=0)
    created_at = models.DateTimeField(blank=False, null=False,
                                      auto_now_add=True)
    actual_before_time = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return '{}, {}'.format(self.price_per_user, self.users_min)


class CompanyPlanedTariff(models.Model):
    company = models.OneToOneField(Company, on_delete=models.CASCADE,
                                   related_name='planned')
    tariff = models.ForeignKey(Tariff, blank=True, null=True,
                               on_delete=models.PROTECT)
    users_planned = models.IntegerField(default=0, blank=False)

    def __str__(self):
        return '{} Planned Tariff'.format(self.company)


class CompanyPrepaid(models.Model):
    company = models.OneToOneField(Company, on_delete=models.CASCADE,
                                   related_name='prepaid')
    balance = MoneyField(max_digits=14, decimal_places=2,
                         default_currency='RUB', default=0)
    prepaid = models.BooleanField(default=False)
    users_prepaid = models.IntegerField(default=5)
    prepaid_till = models.DateTimeField(blank=True, null=True)
    auto_payment = models.BooleanField(default=False)

    def __str__(self):
        return '{} Prepaid'.format(self.company)

    @property
    def balance_usd(self):
        return convert_money(self.balance, 'USD')


class CreditInvoice(models.Model):
    '''
    Can be handled only for same currency as in CompanyPrepaid instance.
    Edit this description when other payments added.
    '''
    company = models.ForeignKey(Company, on_delete=models.PROTECT)
    created_at = models.DateTimeField(blank=False, null=False,
                                      auto_now_add=True)
    value = MoneyField(max_digits=14, decimal_places=2,
                       default_currency='RUB')
    confirmed = models.BooleanField(default=False)
    payment_type = models.IntegerField(choices=CREDIT_CHOICES, default=1)
    comment = models.TextField(max_length=300, null=True, blank=True)

    def __str__(self):
        return '{} Credit Invoice Of {}'.format(self.company, self.value)


class DebitInvoice(models.Model):
    company = models.ForeignKey(Company, on_delete=models.PROTECT)
    tariff = models.ForeignKey(Tariff, blank=False, null=False,
                               on_delete=models.PROTECT)
    created_at = models.DateTimeField(blank=False, null=False,
                                      auto_now_add=True)
    paid_till = models.DateTimeField(blank=False, null=False)
    users_prepaid = models.IntegerField(default=0, blank=False)
    expand_payment = models.BooleanField(default=False)
    confirmed = models.BooleanField(default=False)
    value = MoneyField(max_digits=14, decimal_places=2,
                       default_currency='RUB')

    def __str__(self):
        return '{} Debit Invoice Of {}'.format(self.company, self.value)

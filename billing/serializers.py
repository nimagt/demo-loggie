from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers

from djmoney.contrib.exchange.models import Rate
from .models import Tariff, CompanyPlanedTariff, CompanyPrepaid, \
                    DebitInvoice, CreditInvoice


errors = {
    "tariff_terms": _("Prepaid number of users must match tariff terms."),
    "auto_payment_on": _("Can not create invoice while auto payment is on."),
    "invoice_in_process": _("Invoice is processing."),
    "not_enough_money": _("Not enough money."),
    "company_prepaid": _("Company has a prepaid tariff."),
    "tariff_needed": _("Choose tariff first."),
    }


class ExchangeRateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rate
        fields = ['currency', 'value']


class TariffSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tariff
        fields = '__all__'


class CompanyPlanedTariffSerializer(serializers.ModelSerializer):
    tariff = serializers.PrimaryKeyRelatedField(required=True,
                                                queryset=Tariff.objects.all())

    class Meta:
        model = CompanyPlanedTariff
        fields = '__all__'
        read_only_fields = ['company']

    def validate(self, data):
        if data['users_prepaid'] < data['tariff'].users_min:
            raise serializers.ValidationError(errors['tariff_terms'])
        return data


class CompanyPrepaidSerializer(serializers.ModelSerializer):

    class Meta:
        model = CompanyPrepaid
        fields = '__all__'
        read_only_fields = ('company', 'balance', 'prepaid',
                            'users_prepaid', 'prepaid_till')


class DebitInvoiceSerializer(serializers.ModelSerializer):

    class Meta:
        model = DebitInvoice
        fields = '__all__'
        read_only_fields = ['company', 'created_at', 'value', 'paid_till',
                            'expand_payment', 'confirmed', 'tariff']

    def validate_users_prepaid(self, value):
        if self.context['request'].user.company != value.company:
            raise serializers.ValidationError(errors['not_valid_rate_kit'])
        return value

    def validate(self, data):
        company = self.context['request'].user.company
        if not company.tariff:
            raise serializers.ValidationError(errors['tariff_needed'])
        if company.prepaid.auto_payment:
            raise serializers.ValidationError(errors['auto_payment_on'])
        if DebitInvoice.objects.filter(company=company,
                                       confirmed=False).exists():
            raise serializers.ValidationError(errors['invoice_in_process'])
        if data['users_prepaid'] < company.tariff.users_min:
            raise serializers.ValidationError(errors['tariff_issue'])
        value = data['users_prepaid'] * company.tariff.price_per_user
        if company.prepaid.balance < value:
            raise serializers.ValidationError(errors['not_enough_money'])
        if company.prepaid:
            raise serializers.ValidationError(errors['company_prepaid'])
        return data

    def create(self, validated_data):
        company = self.context['request'].user.company
        users_prepaid = validated_data['users_prepaid']
        price_per_user = company.tariff.price_per_user
        value = users_prepaid * price_per_user
        invoice = DebitInvoice.objects.create(**validated_data,
                                              company=company,
                                              value=value,
                                              tariff=company.tariff)
        return invoice


class CreditInvoiceSerializer(serializers.ModelSerializer):

    class Meta:
        model = CreditInvoice
        fields = '__all__'

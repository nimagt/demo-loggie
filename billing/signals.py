from django.dispatch import receiver
from django.db.models.signals import post_save

from .models import CompanyPlanedTariff, CompanyPrepaid, CreditInvoice
from customers.models import Company


'''
Signals create billing instances on company create
'''


@receiver(post_save, sender=Company)
def create_company_planned_tariff(sender, instance, created, **kwargs):
    if created:
        CompanyPlanedTariff.objects.get_or_create(company=instance)


@receiver(post_save, sender=Company)
def create_company_prepaid(sender, instance, created, **kwargs):
    if created:
        CompanyPrepaid.objects.get_or_create(company=instance)


@receiver(post_save, sender=CreditInvoice)
def confirm_credit_invoice(sender, instance, created, **kwargs):
    if created and instance.payment_type == 0:  # confirm for admin payment
        instance.confirmed = True
        instance.company.prepaid.balance += instance.value
        instance.save()
        instance.company.prepaid.save()

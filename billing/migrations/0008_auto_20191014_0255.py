# Generated by Django 2.2.3 on 2019-10-14 02:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0007_auto_20190818_1108'),
    ]

    operations = [
        migrations.AddField(
            model_name='creditinvoice',
            name='comment',
            field=models.TextField(blank=True, max_length=300, null=True),
        ),
        migrations.AddField(
            model_name='creditinvoice',
            name='confirmed',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='creditinvoice',
            name='payment_type',
            field=models.IntegerField(choices=[(0, 'Made by admin'), (1, 'Credit card'), (2, 'Bank transfer')], default=1),
        ),
        migrations.AddField(
            model_name='debitinvoice',
            name='confirmed',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='companyplanedtariff',
            name='company',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='planed_tariff', to='customers.Company'),
        ),
        migrations.AlterField(
            model_name='companyprepaid',
            name='company',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='prepaid', to='customers.Company'),
        ),
    ]

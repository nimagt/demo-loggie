from django.utils import timezone
from django.db.models import Q
from django.db import transaction

from celery import shared_task
from djmoney.contrib.exchange.backends import FixerBackend

from customers.models import Company
from .models import DebitInvoice


'''
Periodic billing tasks
'''


def get_debit_quere():
    quere = DebitInvoice.objects.filter(confirmed=False)
    return quere


def get_schedule_quere():
    prepaid = Q(prepaid__prepaid=True)
    need_to_pay = Q(prepaid__prepaid_till__lte=timezone.now())
    quere = Company.objects.filter(prepaid & need_to_pay)
    return quere


def confirm_payment(invoice):
    company = invoice.company
    with transaction.atomic():
        if company.prepaid.balance >= invoice.value:
            company.prepaid.balance -= invoice.value
            company.prepaid.users_prepaid = invoice.users_prepaid
            if not company.prepaid.prepaid_till:
                company.prepaid.prepaid_till = timezone.now()
            company.prepaid.prepaid_till += invoice.tariff.periodic
            company.prepaid.prepaid = True
            company.prepaid.save()
            invoice.confirmed = True
            invoice.paid_till = company.prepaid.prepaid_till
            invoice.save()
            return invoice
        return print('its strange')


@shared_task
def create_schedule_invoices():
    for company in get_schedule_quere():
        planned = company.planned
        value = planned.users_planned * planned.tariff.price_per_user
        paid_till = company.prepaid.prepaid_till + planned.tariff.periodic
        fresh_invoice = DebitInvoice.objects.filter(company=company,
                                                    confirmed=False).exists()
        if company.prepaid.auto_payment \
                and company.planned.tariff \
                and company.prepaid.users_prepaid > 0 \
                and company.prepaid.balance >= value \
                and not fresh_invoice:
            DebitInvoice.objects.create(
                company=company,
                tariff=planned.tariff,
                paid_till=paid_till,
                users_prepaid=planned.users_planned,
                value=value)
        else:
            company.prepaid.auto_payment = False
            company.prepaid.prepaid = False
            company.prepaid.prepaid_till = None
            company.prepaid.users_prepaid = 5
            company.prepaid.save()
            print('email for stop prepaid')


@shared_task
def confirm_invoices():
    for invoice in get_debit_quere():
        confirm_payment(invoice)


@shared_task
def update_exchange_rates():
    FixerBackend().update_rates()

from django.contrib import admin

from .models import Tariff, CreditInvoice, DebitInvoice


@admin.register(CreditInvoice)
class CreditInvoiceAdmin(admin.ModelAdmin):
    model = CreditInvoice
    list_display = ['company', 'created_at', 'value', 'confirmed',
                    'payment_type']
    list_filter = ['company', 'created_at', 'value', 'payment_type',
                   'confirmed']
    readonly_fields = ['confirmed']
    search_fields = ['company__name', 'comment']
    autocomplete_fields = ['company']

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False


@admin.register(DebitInvoice)
class DebitInvoiceAdmin(admin.ModelAdmin):
    model = DebitInvoice
    list_display = ['company', 'created_at', 'value', 'confirmed']
    list_filter = ['company', 'created_at', 'value', 'confirmed']
    readonly_fields = ['company', 'value', 'tariff', 'confirmed',
                       'expand_payment', 'users_prepaid', 'paid_till']
    search_fields = ['company__name']
    autocomplete_fields = ['company']

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(Tariff)

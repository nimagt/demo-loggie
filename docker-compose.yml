version: '2.4'

services:

  db:
    image: geographica/postgis
    volumes:
      - ./docker/db/pgdata:/var/lib/postgresql/data
    environment:
      - POSTGRES_PASSWD=postgres
    healthcheck:
        test: ["CMD-SHELL", "pg_isready -U postgres"]
        interval: 5s
        timeout: 5s
        retries: 10


  elasticsearch:
    # sysctl must be set no host level
    # sysctl -w vm.max_map_count=262144
    image: docker.elastic.co/elasticsearch/elasticsearch:7.9.0
    container_name: demo-loggie-elastic
    build:
      context: .
      dockerfile: ./docker/elasticsearch/Dockerfile
    ulimits:
      memlock:
        soft: -1
        hard: -1
    environment:    
      - discovery.type=single-node
      - cluster.name=loggie-cluster
      - bootstrap.memory_lock=true
      # need to be disabled in prod
      - xpack.security.enabled=false
      - "ES_JAVA_OPTS=-Xms1g -Xmx1g"
      - cluster.routing.allocation.disk.watermark.low=93%
      - cluster.routing.allocation.disk.watermark.high=95%
      - cluster.routing.allocation.disk.watermark.flood_stage=99%
    volumes:
      - ./docker/elasticsearch/sysctl.conf:/etc/sysctl.conf
      - esdata:/usr/share/elasticsearch/data
    ports:
      - "9200:9200"
      - "9300:9300"


  web:
    image: loggie-django:latest
    build:
      context: .
      dockerfile: ./docker/django/Dockerfile
    restart: on-failure
    #command: uwsgi --ini docker/django/uwsgi.ini --http-socket 0.0.0.0:8000
    command: python manage.py runserver 0.0.0.0:8000
    sysctls:
        net.core.somaxconn: 1024
    environment:
      - DJANGO_SETTINGS_MODULE=loggie.settings.dev
      - BROKER_URL=amqp://admin:mypass@rabbit:5672/loggie_rabbit
    links:
      - elasticsearch:elasticsearch
      - db:db
    volumes:
      - .:/code
      - ./docker/django/log/:/var/log/uwsgi/
      # - ./docker/django/socket/:/tmp/loggie.sock
    ports:
      - "8000:8000"
    depends_on:
      - db


  rabbit:
    hostname: rabbit
    image: loggie-rabbit:latest
    build:
      context: .
      dockerfile: ./docker/rabbitmq/Dockerfile
    container_name: loggie-rabbit
    environment:
        - RABBITMQ_DEFAULT_USER=admin
        - RABBITMQ_DEFAULT_PASS=mypass
        - RABBITMQ_DEFAULT_VHOST=loggie_rabbit
        - RABBITMQ_VM_MEMORY_HIGH_WATERMARK=0.49
    volumes:
        - ./docker/rabbitmq/data/:/var/lib/rabbitmq
    ports:
      - "5672:5672"
    healthcheck:
        test: ["CMD", "curl", "-f", "localhost:15672"]
        interval: 5s
        timeout: 5s
        retries: 15


  celery-worker:
    image: loggie-django
    restart: on-failure
    container_name: loggie-celery
    command: celery -A loggie worker -l INFO --time-limit=300 --concurrency=2
    #user: 999:users
    environment:
      - DJANGO_SETTINGS_MODULE=loggie.settings.dev
      - BROKER_URL=amqp://admin:mypass@rabbit:5672/loggie_rabbit
    links:
        - rabbit:rabbit
    volumes:
        - .:/code
        - ./docker/celery/:/var/run/celery/
    depends_on:
        rabbit:
            condition: service_healthy
        db:
            condition: service_healthy


  worker-beat:
    image: loggie-django
    restart: on-failure
    container_name: loggie-celery-beat
    command: celery -A loggie beat -l INFO
    environment:
      - DJANGO_SETTINGS_MODULE=loggie.settings.dev
      - BROKER_URL=amqp://admin:mypass@rabbit:5672/loggie_rabbit
    volumes:
        - .:/code
        - ./docker/celery/:/var/run/celery/
    #depends_on:
    #    - celery-worker


  #flower:
  #  image: loggie-django
  #  restart: on-failure
  #  container_name: loggie-flower
  #  command: celery flower -A loggie
  #  environment:
  #    - DJANGO_SETTINGS_MODULE=loggie.settings.dev
  #  links:
  #      - rabbit:rabbit
  #  volumes:
  #      - .:/code
  #  ports:
  #      - "5555:5555"
  #  depends_on:
  #      - worker-beat


volumes:
  pgdata:
    external: true
  rabbitmq:
  esdata:
    external: true
